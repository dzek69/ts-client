import { cwd } from 'process';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import inject from '@rollup/plugin-inject';
import alias from '@rollup/plugin-alias';
import resolve from '@rollup/plugin-node-resolve';
import nodeResolve from '@rollup/plugin-node-resolve';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import terser from '@rollup/plugin-terser';

export default [
  //ESM
  {
    input: './lib/index.js',
    output: {
      dir: './lib/esm',
      format: 'es',
      name: 'filehub',
      sourcemap: true
    },
    plugins: [
      resolve({ browser: true, preferBuiltins: true }),
      commonjs({ transformMixedEsModules: true }),
      inject({ Buffer: ['buffer', 'Buffer'] }),
      json(),
      terser(),
      alias({
        entries: [
          //{ find: 'crypto', replacement: 'crypto-browserify' },
          { find: 'stream', replacement: 'stream-browserify' }
        ]
      })
    ],
    onwarn: function (warning, handler) {
      if (warning.code === 'THIS_IS_UNDEFINED' || warning.code === 'CIRCULAR_DEPENDENCY') {
        return;
      }

      handler(warning);
    }
  },
  //UMD
  {
    input: './lib/index.js',
    output: {
      dir: './lib/umd',
      format: 'umd',
      name: 'filehub',
      sourcemap: true,
      globals: {
        'postchain-client': 'pcl',
        '@chromia/ft4': 'ft4'
      }
    },
    plugins: [
      alias({
        entries: [{ find: /^@filehub\/(.*)/, replacement: `${cwd()}/lib/$1` }]
      }),
      peerDepsExternal(),
      nodeResolve({ browser: true, preferBuiltins: true }),
      commonjs({ transformMixedEsModules: true }),
      json(),
      terser()
    ],
    onwarn: function (warning, handler) {
      if (warning.code === 'THIS_IS_UNDEFINED' || warning.code === 'CIRCULAR_DEPENDENCY') {
        return;
      }

      handler(warning);
    }
  },
  //NODE
  {
    input: './lib/index.js',
    external: ['postchain-client', '@chromia/ft4', 'tiny-typed-emitter', 'chalk', 'crypto'],
    output: {
      dir: './lib/cjs',
      format: 'cjs',
      name: 'filehub',
      sourcemap: true
    },
    plugins: [commonjs(), json(), terser()],
    onwarn: function (warning, handler) {
      if (warning.code === 'THIS_IS_UNDEFINED') {
        return;
      }

      handler(warning);
    }
  }
];
