# Filehub Client

TypeScript client implementation for persisting and reading files on the Chromia blockchain.

## Getting started

Filehub utilises [@chromia/ft4](https://www.npmjs.com/package/@chromia/ft4) and [postchain-client](https://www.npmjs.com/package/postchain-client) as peer dependency.

#### 1. Initialize Filehub

```ts
import { Filehub, FilehubSettings } from 'filehub';

// Settings to initialize Filehub
const settings: FilehubSettings = {
  filehubBRID: filehubBrid,
  filehubUrl: FILEHUB_NODE_API_URL
};
// Instantiates a new Filehub client by its settings
const filehub = new Filehub(settings);

// Debug logging can be enabled, disabled by default
new Filehub(settings, , debugLogging = false);
```

#### 2. Register user

```ts
import * as pcl from "postchain-client";
import * ft4 from "@chromia/ft4";

// Creates a FT4 connection to Filehub
const connection = await filehub.initializeConnection();

const store = ft4.createInMemoryFtKeyStore(userSignature);
const userSignature = pcl.newSignatureProvider({
  privKey: config.userPrivateKey // Filehub user
});
const storeInteractor = ft4.createKeyStoreInteractor(connection.client, store);

const singleSignAuthDescriptor = ft4.createSingleSigAuthDescriptorRegistration(
  [ft4.FlagsType.Account, ft4.FlagsType.Transfer],
  userSignature.pubKey
);
const adminSignature = pcl.newSignatureProvider({
  privKey: config.adminPrivateKey // Filehub admin
});

// Registers the user
await ft4.registerAccount(connection.client, adminSignature, singleSignAuthDescriptor);

const accountsData = await storeInteractor.getAccounts();

// Session will be used for communcation with Filehub
const userSession = await storeInteractor.getSession(accountsData[0]?.id);
```

#### 3. Register event listeners

```ts
filehub.on('onChunkStored', (fileHash: Buffer, totalChunksForFile: number, location: IChunkLocation, error?: Error) => {
  // Implement logic to track progress of file upload
});

filehub.on('onFileStored', (fileHash: Buffer, metadata: FsFileMetadata, error?: Error) => {
  // Implement logic to track when all chunks of a file has been uploaded
});
```

#### 4. Store file

```ts
import * as path from 'path';
import fs from 'fs';

// Read local file
const filepath = path.resolve('./example/demo.txt');
const fileDataAsBuffer = await fs.promises.readFile(filepath);

// Each file must set content-type as metadata
const file = FsFile.fromData(fileDataAsBuffer, {
  ContentType: 'text/plain' // Example
});

// Storing file using FT4 user session & signature
await filehub.storeFile(userSession, userSignature, file);
```

###### Supported content types

- `image/jpeg`
- `image/png`
- `image/gif`
- `image/webp`
- `application/zip`
- `application/pdf`
- `application/octet-stream`
- `application/json`
- `application/xml`
- `application/x-www-form-urlencoded`
- `video/mp4`
- `audio/ogg`
- `audio/mpeg`
- `multipart/form-data`
- `text/html`
- `text/css`
- `text/plain`
- `text/javascript`

#### 5. Fetch file

```ts
import { FsFile } from 'filehub';

// Filehub can validate that file has been stored using it's hash
const hasFile: boolean = await filehub.hasFile(file.hash);

// To retrieve all file hashes a user has uploaded
const hasFile: Buffer[] = await filehub.getAllFileHashes(userSession.account.id);

// Fetching file from Filehub
const storedFile: FsFile = await filehub.getFile(file.hash);

// Fetching file metadata from Filehub
const metadata: Buffer = await filehub.getFileMetadata(file.hash);
```

---

## Administration

When deploying Filehub each underlying Filechain must be registered before Filehub can be user. This management can be handled via the Filehub ts-client as well.

```ts
// Instantiates a new Filehub client by its url and brid
const filehub = new Filehub(filehubUrl, filehubBrid);

// Instantiates a new FilehubAdministrator client by passing the Filehub and admin key pair
const filehubAdministrator = new FilehubAdministrator(filehub, adminKeyPair);

// Register Filechain to Filehub
await filehubAdministrator.registerFilechain(filechainBrid, filechainUrl, filechainOwnerEvmAddress);

// Disable Filechain on Filehub
await filehubAdministrator.disableFilechain(filechainBrid, filechainUrl);

// Enable Filechain on Filehub
await filehubAdministrator.enableFilechain(filechainBrid, filechainUrl);

// Update URL to filechain (operation for admin or filechain owner)
await filehubAdministrator.updateFilechainUrl(ft4Session, filechainBrid, filechainUrl);
```
