import chalk from 'chalk';
export enum LogLevel {
  Debug,
  Info,
  Warning,
  Error
}

export class Logger {
  private readonly level: LogLevel;
  constructor(level: LogLevel) {
    this.level = level;
  }

  private log(message: string): string {
    return `${new Date().toISOString().replace('T', ' ').split('.')[0]} ${this.levelParser()} ${message}`;
  }

  private levelParser() {
    switch (this.level) {
      case LogLevel.Debug:
        return chalk.blue('debug');
      case LogLevel.Warning:
        return chalk.yellow('warn');
      case LogLevel.Error:
        return chalk.red('warn');
      default:
        return chalk.grey('info');
    }
  }

  debug(message: string) {
    if (this.level > LogLevel.Debug) return;

    console.debug(this.log(message));
  }
  info(message: string) {
    if (this.level > LogLevel.Info) return;

    console.info(this.log(message));
  }
  warn(message: string) {
    if (this.level > LogLevel.Warning) return;

    console.warn(this.log(message));
  }
  error(message: string, error?: unknown) {
    console.error(this.log(message), error);
  }
}
