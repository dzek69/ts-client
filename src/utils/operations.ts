import { Operation, encryption } from 'postchain-client';

export function nop(): Operation {
  return { name: 'nop', args: [encryption.randomBytes(32)] };
}
