export interface IChunkLocation {
  idx: number;
  hash: Buffer;
  brid: Buffer;
  url: string;
}

export type ChunkLocation = {
  idx: number;
  hash: Buffer;
  brid: Buffer;
  url: string;
};
