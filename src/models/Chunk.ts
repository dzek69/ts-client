export interface IChunkHashIndex {
  hash: Buffer;
  idx: number;
}

export class ChunkIndex {
  public readonly data: Buffer;
  public readonly idx: number;

  public constructor(data: Buffer, idx: number) {
    this.data = data;
    this.idx = idx;
  }
}
