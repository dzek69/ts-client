import { hashData } from '../utils/crypto.js';
import { ChunkIndex } from './Chunk.js';

export class FsFile {
  private static BYTES: number = 100000;

  public static fromData(data: Buffer, metadata: FsFileMetadata): FsFile {
    const fsFile = new FsFile(data, Buffer.from(JSON.stringify(metadata)));

    return fsFile;
  }

  public static fromChunks(chunks: ChunkIndex[], metadata: Buffer): FsFile {
    const dataChunks: Buffer[] = chunks
      .sort((a: ChunkIndex, b: ChunkIndex) => a.idx - b.idx)
      .map((c: ChunkIndex) => c.data);

    const data = Buffer.concat(dataChunks);
    const fsFile = new FsFile(data, metadata);

    return fsFile;
  }

  private static sliceIntoChunks(data: Buffer): Buffer[] {
    const nrOfChunks = Math.ceil(data.length / FsFile.BYTES);

    const chunks: Buffer[] = [];
    for (let i = 0; i < nrOfChunks; i++) {
      chunks.push(data.slice(i * FsFile.BYTES, (i + 1) * FsFile.BYTES));
    }

    return chunks;
  }

  public readonly hash: Buffer;
  public readonly chunks: Buffer[];
  public readonly size: number;
  public readonly data: Buffer;
  public readonly metadata: Buffer;

  public getMetadata(): FsFileMetadata {
    return JSON.parse(this.metadata.toString());
  }

  private constructor(data: Buffer, metadata: Buffer) {
    this.hash = hashData(data);
    this.data = data;
    this.chunks = FsFile.sliceIntoChunks(data);
    this.size = data.length;
    this.metadata = metadata;
  }

  public getChunk(index: number): Buffer {
    return this.chunks[index];
  }

  public numberOfChunks(): number {
    return Math.ceil(this.size / FsFile.BYTES);
  }
}

export class FsFileMetadata {
  ContentType: ContentType;
}

export type ContentType =
  | 'image/jpeg'
  | 'image/png'
  | 'image/gif'
  | 'image/webp'
  | 'application/zip'
  | 'application/pdf'
  | 'application/octet-stream'
  | 'application/json'
  | 'application/xml'
  | 'application/x-www-form-urlencoded'
  | 'video/mp4'
  | 'audio/ogg'
  | 'audio/mpeg'
  | 'multipart/form-data'
  | 'text/html'
  | 'text/css'
  | 'text/plain'
  | 'text/javascript';
