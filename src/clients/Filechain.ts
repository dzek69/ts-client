import * as pcl from 'postchain-client';
import { nop } from '../utils/operations';
import { Logger } from '../utils/logger';

export default class Filechain {
  private readonly filechainUrl: string;
  private readonly brid: string;
  private chromiaClient: pcl.IClient;
  private logger: Logger;

  constructor(filechainUrl: string, brid: string, logger: Logger) {
    this.filechainUrl = filechainUrl;
    this.brid = brid;
    this.logger = logger;
  }

  async initializeClient(): Promise<pcl.IClient> {
    if (this.chromiaClient) return this.chromiaClient;
    this.chromiaClient = await pcl.createClient({
      nodeUrlPool: this.filechainUrl,
      blockchainRid: this.brid
    });

    return this.chromiaClient;
  }

  async persistChunkData(
    signature: pcl.SignatureProvider,
    iccfProof: pcl.Operation,
    initChunkTx: pcl.RawGtv,
    chunkData: Buffer
  ): Promise<pcl.TransactionReceipt> {
    const client = await this.initializeClient();

    try {
      const transaction: pcl.Transaction = {
        operations: [
          iccfProof,
          {
            name: 'fs.add_chunk_data',
            args: [initChunkTx, chunkData]
          },
          nop()
        ],
        signers: [signature.pubKey]
      };

      const response = await client.signAndSendUniqueTransaction(transaction, signature);

      this.logger.debug(
        `Filechain | Storing of chunks returned status: ${response.status}, with code: ${
          response.statusCode
        } and tx id: ${response.transactionRid.toString('hex')}`
      );

      return response;
    } catch (error: unknown) {
      this.logger.error(`Filechain | Failed store chunk data`, error);

      const chunkExistsError =
        error instanceof pcl.TxRejectedError && error.shortReason.includes('[OK]: Chunk already exists');

      if (chunkExistsError) {
        this.logger.debug(`Filechain | Chunk data already exsits`);

        // Returning successful response since chunk is already persisted
        return {
          status: pcl.ResponseStatus.Confirmed,
          statusCode: 200,
          transactionRid: Buffer.from('')
        };
      }

      throw error;
    }
  }

  async chunkHashExists(hash: string): Promise<boolean> {
    const client = await this.initializeClient();
    return client.query('fs.chunk_hash_exists', { hash });
  }

  async getChunkDataByHash(hash: string): Promise<string> {
    const client = await this.initializeClient();
    return client.query('fs.get_chunk', { hash });
  }
}
