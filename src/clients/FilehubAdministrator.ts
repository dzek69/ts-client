import { Filehub } from './Filehub';
import * as pcl from 'postchain-client';
import { Session } from '@chromia/ft4';
import { nop } from '../utils/operations';

export class FilehubAdministrator {
  private readonly filehub: Filehub;
  private readonly admin: pcl.KeyPair;

  public constructor(filehub: Filehub, admin: pcl.KeyPair) {
    this.filehub = filehub;
    this.admin = admin;
  }

  /**
   * Registers a Filechain to persist files in.
   *
   * @param user that is an admin of the filehub.
   * @param rid of the filechain.
   */
  public async registerFilechain(rid: string, url: string, ownerId: Buffer) {
    const connection = await this.filehub.initializeConnection();
    const adminSignatureProvider = pcl.newSignatureProvider(this.admin);
    return connection.client.signAndSendUniqueTransaction(
      {
        operations: [
          {
            name: 'admin.add_filechain',
            args: [rid, url, ownerId]
          }
        ],
        signers: [adminSignatureProvider.pubKey]
      },
      adminSignatureProvider
    );
  }

  /**
   * Disables a Filechain
   */
  public async disableFilechain(rid: string) {
    const connection = await this.filehub.initializeConnection();
    return connection.client.signAndSendUniqueTransaction(
      {
        name: 'admin.disable_filechain',
        args: [rid]
      },
      this.admin
    );
  }

  /**
   * Enables a Filechain
   */
  public async enableFilechain(rid: string, url: string) {
    const connection = await this.filehub.initializeConnection();
    return connection.client.signAndSendUniqueTransaction(
      {
        name: 'admin.enable_filechain',
        args: [rid, url]
      },
      this.admin
    );
  }

  /**
   * Updates a Filechain URL.
   *
   * @param session FT4 session for the owner of the filechain
   * @param rid of the filechain.
   * @param url new url to configure for the filechain.
   */
  public async updateFilechainUrl(session: Session, rid: string, url: string) {
    return await session
      .transactionBuilder()
      .add({
        name: 'admin.update_filechain_url',
        args: [rid, url]
      })
      .add(nop())
      .buildAndSend();
  }
}
