// Models
import { FsFile, FsFileMetadata } from './../models/FsFile';
import { ChunkIndex, IChunkHashIndex } from '../models/Chunk.js';
import { ChunkLocation, IChunkLocation } from './../models/FilechainLocation.js';
import Filechain from './Filechain.js';
import { FilehubSettings } from '../models/FilehubSettings.js';
// Utils
import { hashData } from '../utils/crypto.js';
import { nop } from '../utils/operations.js';
import { Logger, LogLevel } from '../utils/logger';
// Packages
import * as pcl from 'postchain-client';
import * as ft4 from '@chromia/ft4';
import { OnAnchoredHandlerData, TransactionBuilder } from '@chromia/ft4/dist/utils/transaction-builder/types.js';
import { Config } from '@chromia/ft4/dist/utils/types.js';
import { TypedEmitter } from 'tiny-typed-emitter';

export interface FilehubEvents {
  onChunkStored: (
    fileHash: Buffer,
    totalChunksForFile: number,
    chunkLocation: IChunkLocation,
    error?: Error
  ) => Promise<void>;
  onFileStored: (fileHash: Buffer, metadata: FsFileMetadata, error?: Error) => Promise<void>;
}

export class Filehub extends TypedEmitter<FilehubEvents> {
  private readonly filehubUrl: string;
  private readonly filehubBRID: string;
  private connection: ft4.Connection;

  private config: Config;
  private readonly BATCH_SIZE: number = 100;
  private logger: Logger;

  /**
   * Instantiating Filehub.
   */
  constructor(settings: FilehubSettings, debugLogging = false) {
    super();
    const { filehubUrl, filehubBRID } = settings;
    this.filehubUrl = filehubUrl;
    this.filehubBRID = filehubBRID;

    this.logger = new Logger(debugLogging ? LogLevel.Debug : LogLevel.Info);
  }

  /**
   * Initializes chromiaClient for Filehub
   * @returns chromia client with FT4 connection to Filehub
   */
  public async initializeConnection(): Promise<ft4.Connection> {
    if (this.connection) return this.connection;
    const client = await pcl.createClient({
      nodeUrlPool: this.filehubUrl,
      blockchainRid: this.filehubBRID
    });

    return ft4.createConnection(client);
  }

  private async initializeConfig(session: ft4.Session): Promise<Config> {
    if (this.config) return this.config;

    this.config = await session.getConfig();

    return this.config;
  }

  /**
   * Stores a file. Contacts the Filehub and allocates a chunk, and then persists the data in the correct filechain.
   *
   * @param session FT4 session for filehub
   * @param signature user signature to interact with underlying filechains
   * @param file the file to be uploaded
   */
  public async storeFile(
    session: ft4.Session,
    signature: pcl.SignatureProvider,
    file: FsFile,
    maxRetries?: number
  ): Promise<void> {
    try {
      this.logger.info(`Filehub | Storing file with hash [${file.hash.toString('hex')}]...`);
      const hasFile: boolean = await this.hasFile(file.hash);
      if (!hasFile) {
        await this.executeOperation(
          session,
          {
            name: 'fs.allocate_file',
            args: [file.hash, file.size, file.metadata]
          },
          maxRetries
        );
      } else {
        const validatedChunks = await this.hasValidChunks(file.hash);
        if (validatedChunks) {
          this.logger.info(`Filehub | File with hash [${file.hash.toString('hex')}] is already stored.`);
          return;
        }
      }

      return this.storeChunks(session, signature, file, maxRetries);
    } catch (error) {
      this.logger.error(`Filehub | Error storing file`, error);
      return Promise.reject(error);
    }
  }

  /**
   * Checks if a file exists on Filehub by its hash.
   *
   * @param hash of retrieving file.
   */
  public async hasFile(hash: Buffer): Promise<boolean> {
    const client = await this.initializeConnection();
    return client.query<boolean>('fs.has_file', { file_hash: hash.toString('hex') });
  }

  /**
   * Checks if a file has all chunks allocated and initialized on Filehub
   *
   * @param hash of retrieving file.
   */
  public async hasValidChunks(hash: Buffer): Promise<boolean> {
    const client = await this.initializeConnection();
    const validatedFileOnFilehub = client.query<boolean>('fs.validate_file', { file_hash: hash.toString('hex') });

    if (!validatedFileOnFilehub) return false;

    try {
      const chunkExistsOnFilechainPromises: Promise<boolean>[] = [];
      const filechainLocations = await this.getChunkLocations(hash);
      for (const chunkLocation of filechainLocations) {
        const filechain: Filechain = this.initFilechainClient(chunkLocation.url, chunkLocation.brid.toString('hex'));

        chunkExistsOnFilechainPromises.push(filechain.chunkHashExists(chunkLocation.hash.toString('hex')));
      }

      return (await Promise.all(chunkExistsOnFilechainPromises)).every((x) => x == true);
    } catch (error: unknown) {
      const chunksNotYetAllocatedError =
        error instanceof Error && error.message.includes('Did not receive enough active & online Filechains');

      if (chunksNotYetAllocatedError) return false;

      throw error;
    }
  }

  /**
   * Retrieves metadata of file from Filehub by its hash.
   *
   * @param hash of retrieving file.
   */
  public async getFileMetadata(hash: Buffer): Promise<Buffer> {
    const client = await this.initializeConnection();
    return client.query<Buffer>('fs.get_file_metadata', { file_hash: hash.toString('hex') });
  }

  /**
   * Retrieves a file by its hash.
   *
   * @param hash of retrieving file.
   */
  public async getFile(hash: Buffer, gatewayTranslator?: (host: string) => string): Promise<FsFile> {
    const hasFile = await this.hasFile(hash);
    if (!hasFile) {
      const msg = 'Filehub | No file stored which matches the given hash';
      this.logger.error(msg);
      throw new Error(msg);
    }

    const validatedChunks = await this.hasValidChunks(hash);
    if (!validatedChunks) {
      const msg =
        'Filehub | File not complete. Some chunks might not yet be allocated, initialized or stored on Filechain';
      this.logger.error(msg);
      throw new Error(msg);
    }

    try {
      const metadata = await this.getFileMetadata(hash);
      const filechainLocations = await this.getChunkLocations(hash);
      const promises: Promise<ChunkIndex>[] = [];
      for (const chunkLocation of filechainLocations) {
        this.logger.debug(
          `Filehub | Getting chunk ${chunkLocation.hash.toString('hex')} from filechain: ${chunkLocation.url}`
        );
        if (gatewayTranslator) {
          chunkLocation.url = gatewayTranslator(chunkLocation.url);
        }
        const filechain: Filechain = this.initFilechainClient(chunkLocation.url, chunkLocation.brid.toString('hex'));
        promises.push(this.getChunk(filechain, chunkLocation));
      }

      const chunkIndexes = await Promise.all(promises);
      return new Promise((resolve) => resolve(FsFile.fromChunks(chunkIndexes, metadata)));
    } catch (error) {
      this.logger.error(`Filehub | Error retrieving file`, error);
      const reject: Promise<FsFile> = Promise.reject(error);
      return reject;
    }
  }

  /**
   * Retrieves all file hashes for an account.
   *
   * @param id of account.
   */
  public async getAllFileHashes(accountId: Buffer): Promise<Buffer[]> {
    const client = await this.initializeConnection();
    return client.query<Buffer[]>('fs.get_all_file_hashes', { account_id: accountId });
  }

  private initFilechainClient(url: string, brid: string): Filechain {
    return new Filechain(url, brid, this.logger);
  }

  private async storeChunks(
    session: ft4.Session,
    signature: pcl.SignatureProvider,
    file: FsFile,
    maxRetries?: number
  ): Promise<void> {
    const allocatedcChunksResult = await this.allocateChunks(session, file, maxRetries);

    if (!allocatedcChunksResult) throw new Error('Filehub | Failed to allocated chunks');

    const chunkLocations = await this.getChunkLocations(file.hash);

    const initializedChunksResult = await this.initializeChunks(session, signature, file, chunkLocations, maxRetries);

    if (!initializedChunksResult) throw new Error('Filehub | Failed to initialize chunks');

    this.logger.info(`Filehub | All chunks stored and initialized for file with hash [${file.hash.toString('hex')}]`);
  }

  private async getChunkLocations(hash: Buffer): Promise<IChunkLocation[]> {
    const client = await this.initializeConnection();
    const chunkLocations = await client.query<ChunkLocation[]>('fs.get_chunk_locations', {
      file_hash: hash.toString('hex')
    });

    this.logger.debug(`Filehub | Got number of chunks: ${chunkLocations.length}`);
    if (chunkLocations.length < 1) {
      throw new Error('Filehub | Did not receive enough active & online Filechains');
    }

    return chunkLocations;
  }

  private async getChunk(filechain: Filechain, chunkHash: IChunkHashIndex): Promise<ChunkIndex> {
    const data = await this.getChunkDataByHash(filechain, chunkHash.hash);
    return new ChunkIndex(Buffer.from(data, 'hex'), chunkHash.idx);
  }

  private getChunkDataByHash(filechain: Filechain, hash: Buffer): Promise<string> {
    return filechain.getChunkDataByHash(hash.toString('hex'));
  }

  private async isChunkAllocated(hash: Buffer): Promise<boolean> {
    const client = await this.initializeConnection();
    return await client.query<boolean>('fs.is_chunk_allocated', { hash: hash });
  }

  private async isChunkInitalized(hash: Buffer): Promise<boolean> {
    const client = await this.initializeConnection();
    return await client.query<boolean>('fs.is_chunk_initalized', { hash: hash });
  }

  private async allocateChunks(session: ft4.Session, file: FsFile, maxRetries?: number): Promise<boolean> {
    this.logger.debug(`Filehub | Storing nr of chunks: ${file.numberOfChunks()}`);
    let transactionBuilder: TransactionBuilder = session.transactionBuilder();
    const config: Config = await this.initializeConfig(session);
    let operationsCount = 0;
    for (let i = 0; i < file.numberOfChunks(); i++) {
      const chunk = file.getChunk(i);
      const chunkHash = hashData(chunk);

      if (!(await this.isChunkAllocated(chunkHash))) {
        this.logger.debug(
          `Filehub | Allocating chunk with hash ${chunkHash.toString('hex')} for file with hash ${file.hash.toString(
            'hex'
          )}`
        );
        transactionBuilder = transactionBuilder.add({
          name: 'fs.allocate_chunk',
          args: [file.hash, chunkHash, chunk.length, i]
        });
        operationsCount++;
      }

      if (operationsCount > this.BATCH_SIZE) {
        const txReceipt = await this.executeTransaction(transactionBuilder, config, maxRetries);
        if (txReceipt.receipt.status != pcl.ResponseStatus.Confirmed) return false;

        operationsCount = 0;
      }
    }

    const txUnsigned = await transactionBuilder.buildUnsigned();
    if (txUnsigned.operations.length > 0) {
      const txReceipt = await this.executeTransaction(transactionBuilder, config, maxRetries);
      if (txReceipt.receipt.status != pcl.ResponseStatus.Confirmed) return false;
    }

    return true;
  }

  private async initializeChunks(
    session: ft4.Session,
    signature: pcl.SignatureProvider,
    file: FsFile,
    chunkLocations: IChunkLocation[],
    maxRetries?: number
  ): Promise<boolean> {
    for (const chunkLocation of chunkLocations) {
      const chunk = file.getChunk(chunkLocation.idx);
      const chunkHash = hashData(chunk);

      if (await this.isChunkInitalized(chunkLocation.hash)) {
        const filechain: Filechain = this.initFilechainClient(chunkLocation.url, chunkLocation.brid.toString('hex'));

        // In case the chunk has been allocated and initialized on Filehub but chunk data hasn't been stored on Filechain
        // (could happen when upload is interrupted) - the chunk is reset and then initialized again (ICCF proof needed)
        if (!(await filechain.chunkHashExists(chunkLocation.hash.toString('hex')))) {
          await this.resetChunk(session, chunkLocation, maxRetries);
        } else {
          continue;
        }
      }

      this.logger.debug(
        `Filehub | Initialising chunk with hash ${chunkHash.toString(
          'hex'
        )}, Filechain BRID: ${chunkLocation.brid.toString('hex')}`
      );

      const operation = {
        name: 'fs.init_chunk',
        args: [chunkHash, chunkLocation.brid]
      };

      const txReceipt = await this.executeOperation(
        session,
        operation,
        maxRetries,
        (data: OnAnchoredHandlerData | null, error: Error | null) => {
          return this.persistInFilechain(data, error, session, signature, file, chunk, chunkLocation);
        },
        true
      );
      if (txReceipt.receipt.status != pcl.ResponseStatus.Confirmed) return false;
    }
    return true;
  }

  private async resetChunk(session: ft4.Session, chunkLocation: IChunkLocation, maxRetries?: number): Promise<boolean> {
    if (await this.isChunkInitalized(chunkLocation.hash)) {
      this.logger.debug(
        `Filehub | Restting chunk with hash ${chunkLocation.hash.toString(
          'hex'
        )}, Filechain BRID: ${chunkLocation.brid.toString('hex')}`
      );

      const operation = {
        name: 'fs.reset_chunk',
        args: [chunkLocation.hash, chunkLocation.brid]
      };

      const txReceipt = await this.executeOperation(session, operation, maxRetries, undefined, true);
      if (txReceipt.receipt.status != pcl.ResponseStatus.Confirmed) return false;
    }

    return true;
  }

  private async validateTxStatus(session: ft4.Session, txRid: Buffer): Promise<pcl.ResponseStatus> {
    let txStatus = pcl.ResponseStatus.Waiting;

    while (txStatus == pcl.ResponseStatus.Waiting) {
      await new Promise((resolve) => setTimeout(resolve, 500));

      const txResponse = await session.client.getTransactionStatus(txRid);
      txStatus = txResponse.status;
    }

    return txStatus;
  }

  private async persistInFilechain(
    data: OnAnchoredHandlerData | null,
    error: Error | null,
    session: ft4.Session,
    signature: pcl.SignatureProvider,
    file: FsFile,
    chunk: Buffer,
    chunkLocation: ChunkLocation
  ) {
    try {
      if (error || !data) {
        throw error ?? new Error('Filehub | Data for anchoring tx is null');
      }

      const iccfProof = await data.createProof(this.filehubBRID);
      const filechain: Filechain = this.initFilechainClient(chunkLocation.url, chunkLocation.brid.toString('hex'));

      const persistChunkResponse = await filechain.persistChunkData(signature, iccfProof, data.tx, chunk);
      let txStatus = persistChunkResponse.status;

      if (persistChunkResponse.status == pcl.ResponseStatus.Waiting) {
        txStatus = await this.validateTxStatus(session, persistChunkResponse.transactionRid);
      }

      if (txStatus == pcl.ResponseStatus.Rejected) {
        throw new Error('Filehub | Persist chunk in Filechain rejected');
      }

      this.emit('onChunkStored', file.hash, file.numberOfChunks(), chunkLocation);

      if (chunkLocation.idx + 1 == file.numberOfChunks()) {
        this.emit('onFileStored', file.hash, file.getMetadata());
      }
    } catch (error: unknown) {
      this.logger.error('Filehub | Error persisting chunk in filechain', error);

      // Resetting chunk status to Not Initialized in order to be able to re-upload file
      await this.resetChunk(session, chunkLocation);

      let emitError = new Error('Filehub | Error persisting chunk in Filechain' + error);
      if (error instanceof Error) {
        emitError = error;
      }

      this.emit('onChunkStored', file.hash, file.numberOfChunks(), chunkLocation, emitError);
      this.emit('onFileStored', file.hash, file.getMetadata(), emitError);
    }

    return;
  }
  /**
   * Executes a operation towards the Filehub.
   *
   * @param signer to sign the operation.
   * @param operation to perform.
   */
  private async executeOperation(
    session: ft4.Session,
    operation: pcl.Operation,
    maxRetries: number = 5,
    callback?: (data: OnAnchoredHandlerData | null, error: Error | null) => Promise<void>,
    addNop: boolean = false,
    retryCount: number = 0
  ): Promise<{
    tx: pcl.SignedTransaction;
    receipt: pcl.TransactionReceipt;
  }> {
    try {
      const transactionBuilder = session.transactionBuilder().add(operation, callback);

      if (addNop) {
        transactionBuilder.add(nop());
      }

      this.logger.debug(`Filehub | Executing operation: [${operation.name}]`);
      const tx = await transactionBuilder.buildAndSend();
      this.logger.debug(`Filehub | Operation: [${tx.receipt.status}]`);

      return tx;
    } catch (error: unknown) {
      this.logger.error(`Filehub | Operation failed: [${operation.name}]`);
      const isRateLimitError =
        error instanceof pcl.TxRejectedError && error.shortReason.includes('Insufficient rate limiter points');
      const shouldRetryOperation = retryCount < maxRetries;

      if (isRateLimitError && shouldRetryOperation) {
        // Retry
        const config = await session.getConfig();
        const retry = retryCount + 1;
        const backoffTime = config.rateLimit.recoveryTime * retry;

        this.logger.error(`Filehub | Rate limiter error. Scheduled retry in [${backoffTime}] ms. Attempt: ${retry}`);
        await new Promise((resolve) => setTimeout(resolve, backoffTime));
        return Promise.resolve(this.executeOperation(session, operation, maxRetries, undefined, addNop, retry));
      }

      this.logger.error('Filehub | Error [executeOperation]', error);
      return Promise.reject(error);
    }
  }

  private async executeTransaction(
    txBuilder: TransactionBuilder,
    config: Config,
    maxRetries: number = 5,
    addNop: boolean = false,
    retryCount: number = 0
  ): Promise<{
    tx: pcl.SignedTransaction;
    receipt: pcl.TransactionReceipt;
  }> {
    try {
      if (addNop) {
        txBuilder.add(nop());
      }

      this.logger.debug(`Filehub | Executing transaction`);
      const tx = await txBuilder.buildAndSend();
      this.logger.debug(`Filehub | transaction: [${tx.receipt.status}]`);

      return tx;
    } catch (error: unknown) {
      const isRateLimitError =
        error instanceof pcl.TxRejectedError && error.shortReason.includes('Insufficient rate limiter points');
      const shouldRetryOperation = retryCount < maxRetries;

      if (isRateLimitError && shouldRetryOperation) {
        // Retry
        const retry = retryCount + 1;
        const backoffTime = config.rateLimit.recoveryTime * retry;

        this.logger.error(`Filehub | Rate limiter error. Scheduled retry in [${backoffTime}] ms. Attempt: ${retry}`);
        await new Promise((resolve) => setTimeout(resolve, backoffTime));
        return Promise.resolve(this.executeTransaction(txBuilder, config, maxRetries, addNop, retry));
      }

      const isChunkAlreadyAllocatedError =
        error instanceof pcl.TxRejectedError && error.shortReason.includes('[OK] No work to be done');
      if (isChunkAlreadyAllocatedError) {
        return Promise.resolve({
          tx: Buffer.from(''),
          receipt: {
            status: pcl.ResponseStatus.Confirmed,
            statusCode: 200,
            transactionRid: Buffer.from('')
          }
        });
      }

      this.logger.error('Filehub | Error [executeTransaction]', error);
      return Promise.reject(error);
    }
  }
}
